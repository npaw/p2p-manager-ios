//
//  P2PManager.swift
//  P2PManager
//
//  Created by Elisabet Massó on 7/2/22.
//  Copyright © 2022 NPAW. All rights reserved.
//

import Foundation
import WebRTC
import Utils

public class P2PManager {
    
    private let queue = DispatchQueue(label: "p2p-manager")
    
    private var hashId: String?
    private var config: P2PConfig
    
    enum ConnectionState {
        case new
        case connecting
        case connected
        case failed
    }
    
    // WebRTC Connection
    var peerId: String?
    private var state: ConnectionState
    private(set) var peersMap: [String: PeerConnection]?
    private var sentOffers: [String: PeerConnection]?
    private var factory: RTCPeerConnectionFactory?
    
    // P2P Settings
    private var offerTimeout: Double = 30
    private var maxPeers = 10
    
    // Tracker
    public var trackerUrl: String?
    private var resource: String?
    var webSocketConnected = false
    private lazy var wsProvider: WebSocketCommunication = StarscreamWebSocket(url: URL(string: deafultTracker)!)
    
    private var deafultTracker = "wss://p2p-tracker.youbora.com/webrtc"
    
    // Concurrency
    private weak var timer: Timer?
    private var timeout: Timer?
    
    public init(hashId: String, config: P2PConfig, peerId: String? = nil) {
        self.hashId = Util.sha256(str: hashId)
        self.config = config
        state = .new
        self.peerId = peerId ?? Util.randomId()
        peersMap = [String: PeerConnection]()
        sentOffers = [String: PeerConnection]()
    }
    
    func buildTrackerURL() -> URL? {
        guard let trackerURL = config.tracker, var url = URLComponents(string: trackerURL) else {
            Log.error(tag: self, "Could not convert tracker to URL object.")
            return nil
        }
        if let accountCode = config.accountCode {
            if url.queryItems == nil {
                url.queryItems = []
            }
            url.queryItems?.append(URLQueryItem(name: "accountCode", value: accountCode))
        }
        return url.url
    }
    
    
    /// Initializes the tracker connection and prepares `RTCPeerConnectionFactory`.
    public func start() {
        if trackerUrl.isEmpty {
            trackerUrl = buildTrackerURL()?.absoluteString ?? deafultTracker
        }
        
        guard let url = URL(string: trackerUrl!) else {
            Log.error("Could not convert trackerUrl to URL object.")
            return
        }
        
        if #available(iOS 13.0, *) {
            wsProvider = NativeWebSocket(url: url)
        } else {
            wsProvider = StarscreamWebSocket(url: url)
        }
        
        wsProvider.delegate = self
        wsProvider.connect()
    }
    
    public func stop() {
//        disconnectPeers()
        wsProvider.disconnect()
    }
    
    /// Joins as `Peer for specific content.
    /// Prepares data to send `join` to websocket.
    public func join() {
//        disconnectPeers()
        state = .connecting
        var join = Message()
        join.action = .join
        join.info_hash = hashId
        if !webSocketConnected {
            _ = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) { timer in
                if self.webSocketConnected {
                    self.sendWSMessage(join)
                    timer.invalidate()
                }
            }
        } else {
            sendWSMessage(join)
        }
    }
    
    /// Prepares data to send `offers` to websocket.
    private func sendOffers(_ offers: [Offer], infoHash: String) {
        var message = Message()
        message.action = .offers
        message.info_hash = infoHash
        var mOffers = [SubMessage]()
        for offer in offers {
            var mOffer = SubMessage()
            mOffer.type = .offer
            mOffer.offer_id = offer.id
            mOffer.sdp = offer.sdp
            mOffers.append(mOffer)
        }
        message.offers = mOffers
        sendWSMessage(message)
    }
    
    func disconnectPeers() {
        guard peersMap != nil else {
            return
        }

        for peer in peersMap!.values {
            peer.close()
        }
        
        peersMap!.removeAll()
        sentOffers?.removeAll()
        Log.debug(tag: self, "Disconnect from peers.")
    }
    
    private func handleJoin(message msg: [String : Any]) {
        if let status = msg["status"] as? String {
            if status == "ok", let interval = msg["interval"] as? Int, let infoHash = msg["info_hash"] as? String {
                startOffering(interval: interval, infoHash: infoHash)
            } else if status == "error" {
                state = .failed
            }
        }
    }
    
    private func handleOffer(with msg: Message) {
        guard let peerId = msg.peer_id, let infoHash = msg.info_hash else {
            Log.error(tag: self, "Cannot handle offer, there is no peer id or info hash.")
            return
        }

        if peersMap?[peerId] != nil {
            Log.notice(tag: self, "Discarding offer, already connected to \(peerId).")
            return
        }
        
        guard let offerId = msg.offer?.offer_id, let sdp = msg.offer?.sdp, let type = msg.offer?.type?.toRTCSdpType() else {
            Log.error(tag: self, "Cannot handle offer, some data is missing.")
            return
        }
        
        let offerSdp = RTCSessionDescription(type: type, sdp: sdp)
        
        let peer = PeerConnection(offerId: offerId, isInitiator: false, iceServers: config.iceServers ?? Constants.iceServers, delegate: self)
        peer.id = peerId
        
        if let factory = factory {
            peer.connect(factory: factory, infoHash: infoHash, isReconnecting: false)
        }
        
        peer.setRemoteDescription(offerSdp) { error in
            guard error == nil else {
                Log.error(tag: self, "Error creating remote description: \(error!.localizedDescription)")
                return
            }
            peer.createAnswer { sdp in
                guard let sdp = sdp?.sdp, sdp != "" else {
                    Log.error(tag: self, "Could not get answer sdp.")
                    return
                }
                self.peersMap?[peerId] = peer
                
                self.sendWsAnswer(infoHash: infoHash, toPeerId: peerId, offerId: offerId, sdp: sdp)
            }
            
        }
        
    }
    
    private func handleAnswer(with msg: Message) {
        guard let peerId = msg.peer_id, let offerId = msg.answer?.offer_id else {
            Log.error(tag: self, "Cannot handle answer, there is no peer id or answer.")
            return
        }
        
        let peer = sentOffers?[offerId]
        
        guard let peer = peer else {
            Log.error(tag: self, "Failed to find peer for offer_id \(offerId).")
            return
        }
        
        peer.id = peerId
        
        peersMap?[peerId] = peer
        sentOffers?.removeValue(forKey: peerId)
        
        guard let sdp = msg.answer?.sdp, let type = msg.answer?.type?.toRTCSdpType() else {
            Log.error(tag: self, "Cannot handle offer, some data is missing.")
            return
        }
        
        let answerSdp = RTCSessionDescription(type: type, sdp: sdp)
        peer.setRemoteDescription(answerSdp) { error in
            if let error = error {
                Log.error(tag: self, "Error creating remote description: \(error.localizedDescription)")
                return
            }
        }
        
    }
    
    private func handleCandidates(message msg: Message) {
        guard let peerId = msg.peer_id else {
            Log.error(tag: self, "Cannot handle candidates, there is no peer id.")
            return
        }
        
        var peer = peersMap?[peerId]
        
        if peer == nil {
            let offerId = msg.offer_id
            peer = sentOffers?[offerId ?? ""]
            if peer == nil {
                Log.error(tag: self, "Failed to find peer for peer_id \(peerId) or offer_id \(offerId ?? "nil").")
                return
            }
        }
        
        if let candidates = msg.candidates {
            for candidate in candidates {
                let iceCandidate = RTCIceCandidate(sdp: candidate.candidate ?? "", sdpMLineIndex: candidate.label ?? 0, sdpMid: candidate.id)
                
                peer?.addIceCandidate(iceCandidate, completion: { error in
                    if error != nil {
                        Log.error(tag: self, "Error adding candidate to peer: \(error!.localizedDescription)")
                    }
                })
            }
        }
        
    }
    
    private func startOffering(interval: Int, infoHash: String) {
        state = .connected
        let offerInterval = max(Double(interval), offerTimeout + 5)
        timer?.invalidate()
        timer = nil
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(withTimeInterval: offerInterval, repeats: true) { timer in
                if let peersMapCount = self.peersMap?.count {
                    if peersMapCount < self.maxPeers {
                        self.prepareOffers(infoHash: infoHash, offersPerAnnounce: self.config.announcePeers ?? Constants.announcePeers)
                    } else {
                        Log.debug(tag: self, "Won't create new offers, Peer Max-Size Reached!")
                    }
                } else {
                    self.prepareOffers(infoHash: infoHash, offersPerAnnounce: self.config.announcePeers ?? Constants.announcePeers)
                }
            }
        }
        prepareOffers(infoHash: infoHash, offersPerAnnounce: self.config.announcePeers ?? Constants.announcePeers)
    }
    
    @objc
    private func prepareOffers(infoHash: String, offersPerAnnounce: Int) {
        var offers = [Offer]()
        
        queue.async { [weak self] in
            guard let self = self else { return }
            let group = DispatchGroup()
            
            for _ in 0..<offersPerAnnounce {
                let offerId = Util.randomId()
                Log.debug(tag: self, "Creating offer \(offerId).")
                
                let peer = PeerConnection(offerId: offerId, isInitiator: true, iceServers: self.config.iceServers ?? Constants.iceServers, delegate: self)
                
                if let factory = self.factory {
                    peer.connect(factory: factory, infoHash: infoHash, isReconnecting: false)
                }
                
                group.enter()
                
                peer.createOffer { offer in
                    guard let offer = offer else {
                        Log.error(tag: self, "Could not create offer.")
                        group.leave()
                        return
                    }
                    offers.append(offer)
                    self.sentOffers?[offer.id] = peer

                    DispatchQueue.main.async {
                        self.timeout = Timer.scheduledTimer(withTimeInterval: self.offerTimeout, repeats: false) { timer in
                            if !peer.isConnected() {
                                Log.debug(tag: self, "Deleting peer offer_id \(offer.id), never connected.")
                                self.sentOffers?.removeValue(forKey: offer.id)
                            }
                            self.timeout?.invalidate()
                            self.timeout = nil
                        }
                    }
                    
                    group.leave()
                    
                }
                
            }
            
            self.sendOffers(offers, infoHash: infoHash)
            
        }
        
    }
    
    private func sendWSMessage(_ message: Message) {
        var msg = message
        msg.peer_id = peerId
        msg.token = config.token
        
        do {
            let jsonEncoder = JSONEncoder()
            let jsonData = try jsonEncoder.encode(msg)
            Log.notice(tag: self, "Sending web socket message: \(jsonData.json)")
            wsProvider.send(data: jsonData)
        } catch let error {
            Log.error(tag: self, "Could not create message to send with error: \(error)")
        }
    }
    
    public func sendWsAnswer(infoHash: String, toPeerId: String, offerId: String, sdp: String) {
        var message = Message()
        message.action = .answer
        message.info_hash = infoHash
        message.to_peer_id = toPeerId
        var mAnswer = SubMessage()
        mAnswer.type = .answer
        mAnswer.offer_id = offerId
        mAnswer.sdp = sdp
        message.answer = mAnswer
        sendWSMessage(message)
    }
    
}

extension P2PManager: PeerConnectionDelegate {
    
    public func peerConnectioDidReceiveEvent(_ event: PeerConnectionEvent, id: String, message: Message?, for peer: PeerConnection?) {
        switch event {
            case .connected:
                break
            case .disconnected:
                peersMap?.removeValue(forKey: id)
            case .failed:
                state = .failed
                peersMap?.removeValue(forKey: id)
            case .sendToTracker:
                guard let message = message else { return }
                sendWSMessage(message)
            case .emit:
                break
        }
    }
    
}

extension P2PManager: WebSocketCommunicationDelegate {
    
    func webSocketDidConnect(_ webSocket: WebSocketCommunication) {
        Log.debug(tag: self, "New web socket connection opened.")
        factory = RTCPeerConnectionFactory()
        webSocketConnected = true
        join()
    }
    
    func webSocketDidDisconnect(_ webSocket: WebSocketCommunication) {
        Log.error(tag: self, "Web socket did disconnect.")
    }
    
    func webSocket(_ webSocket: WebSocketCommunication, didReceiveData data: Data) {
        webSocketDataReceived(data)
    }
    
    func webSocket(_ webSocket: WebSocketCommunication, didReceiveError error: String) {
        Log.error(tag: self, "Web socket did received error: \(error)")
    }
    
    func webSocketDataReceived(_ data: Data) {
        guard let message = try? Message(data: data) else { return }
        if let action = message.action {
            Log.notice(tag: self, "Received web socket message: \(data.json)")
            switch action {
                case .join: handleJoin(message: data.dictionary)
                case .offer:
                    if let peersMap = peersMap {
                        if peersMap.count < maxPeers {
                            handleOffer(with: message)
                        } else {
                            Log.debug(tag: self, "Received Offer ignored, Peer Max-Size Reached!")
                        }
                    } else {
                        handleOffer(with: message)
                    }
                case .answer: handleAnswer(with: message)
                case .candidates: handleCandidates(message: message)
                default: break
            }
        }
    }
    
}
