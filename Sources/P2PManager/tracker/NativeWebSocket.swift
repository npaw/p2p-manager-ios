//
//  NativeWebSocket.swift
//  P2PManager
//
//  Created by Elisabet Massó on 8/2/22.
//  Copyright © 2022 NPAW. All rights reserved.
//

import Foundation
import Utils

@available(iOS 13.0, *)
class NativeWebSocket: NSObject, WebSocketCommunication {
    
    var delegate: WebSocketCommunicationDelegate?
    private let url: URL
    private var socket: URLSessionWebSocketTask?
    private lazy var urlSession: URLSession = URLSession(configuration: .default, delegate: self, delegateQueue: nil)

    init(url: URL) {
        self.url = url
        super.init()
    }

    func connect() {
        let socketTask = urlSession.webSocketTask(with: url)
        socketTask.resume()
        socket = socketTask
        readMessage()
    }

    func send(data: Data) {
        socket?.send(.string(String(data: data, encoding: .utf8)!), completionHandler: { error in
            if let error = error {
                Log.error(tag: self, "Could not send data to WebSocket: \(error.localizedDescription)")
            }
        })
    }
    
    private func readMessage() {
        socket?.receive { [weak self] message in
            guard let self = self else { return }
            
            switch message {
                case .success(.data(let data)):
                    self.delegate?.webSocket(self, didReceiveData: data)
                
                case .success(.string(let string)):
                    self.delegate?.webSocket(self, didReceiveData: string.data)
                    
                case .success:
                    let error = "Could not decode reveived data: Received unkown data format."
                    Log.warn(tag: self, "Expected to receive data format but received a string. Check the websocket server config.")
                    self.delegate?.webSocket(self, didReceiveError: error)

                case .failure(let error):
                    self.delegate?.webSocket(self, didReceiveError: error.localizedDescription)
                    self.disconnect()
            }
            self.readMessage()
        }
    }
    
    func disconnect() {
        Log.verbose(tag: self, "Disconnected.")
        socket?.cancel()
        socket = nil
        delegate?.webSocketDidDisconnect(self)
    }
}

@available(iOS 13.0, *)
extension NativeWebSocket: URLSessionWebSocketDelegate, URLSessionDelegate  {
    
    func urlSession(_ session: URLSession, webSocketTask: URLSessionWebSocketTask, didOpenWithProtocol protocol: String?) {
        Log.verbose(tag: self, "Connected.")
        delegate?.webSocketDidConnect(self)
    }
    
    func urlSession(_ session: URLSession, webSocketTask: URLSessionWebSocketTask, didCloseWith closeCode: URLSessionWebSocketTask.CloseCode, reason: Data?) {
        disconnect()
    }
    
}
