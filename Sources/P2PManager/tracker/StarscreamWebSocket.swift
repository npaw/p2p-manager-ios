//
//  StarscreamWebSocket.swift
//  P2PManager
//
//  Created by Elisabet Massó on 8/2/22.
//  Copyright © 2022 NPAW. All rights reserved.
//

import Foundation
import Starscream

class StarscreamWebSocket: WebSocketCommunication {

    var delegate: WebSocketCommunicationDelegate?
    private let socket: WebSocket
    
    init(url: URL) {
        socket = WebSocket(url: url)
        socket.delegate = self
    }
    
    func connect() {
        socket.connect()
    }
    
    func send(data: Data) {
        socket.write(string: String(data: data, encoding: .utf8)!)
    }
    
    func disconnect() {
        socket.disconnect()
    }
}

extension StarscreamWebSocket: Starscream.WebSocketDelegate {
    func websocketDidConnect(socket: WebSocketClient) {
        delegate?.webSocketDidConnect(self)
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        delegate?.webSocketDidDisconnect(self)
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        delegate?.webSocket(self, didReceiveData: text.data)
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        delegate?.webSocket(self, didReceiveData: data)
    }
    
}
