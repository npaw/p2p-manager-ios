//
//  WebSocketCommunication.swift
//  P2PManager
//
//  Created by Elisabet Massó on 8/2/22.
//  Copyright © 2022 NPAW. All rights reserved.
//

import Foundation

protocol WebSocketCommunication: AnyObject {
    var delegate: WebSocketCommunicationDelegate? { get set }
    func connect()
    func disconnect()
    func send(data: Data)
}

protocol WebSocketCommunicationDelegate: AnyObject {
    func webSocketDidConnect(_ webSocket: WebSocketCommunication)
    func webSocketDidDisconnect(_ webSocket: WebSocketCommunication)
    func webSocket(_ webSocket: WebSocketCommunication, didReceiveData data: Data)
    func webSocket(_ webSocket: WebSocketCommunication, didReceiveError error: String)
}
