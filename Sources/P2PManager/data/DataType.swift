//
//  DataType.swift
//  P2PManager
//
//  Created by Elisabet Massó on 7/2/22.
//  Copyright © 2022 NPAW. All rights reserved.
//

import Foundation

enum DataType: Int8 {
    
    case unknown = -1
    case data = 0
    case message = 1
    case bandwidth = 2

    func byteValue() -> UInt8 {
        switch self {
            case .unknown:
                return byteArray(from: -1)[0]
            case .data:
                return byteArray(from: 0)[0]
            case .message:
                return byteArray(from: 1)[0]
            case .bandwidth:
                return byteArray(from: 2)[0]
        }
    }
    
    private func byteArray<T>(from value: T) -> Data where T: FixedWidthInteger {
        return withUnsafeBytes(of: value) { Data($0) }
    }
    
}

extension DataType: CustomStringConvertible {
    public var description: String {
        switch self {
            case .unknown:      return "unknown"
            case .data:         return "data"
            case .message:      return "message"
            case .bandwidth:    return "bandwidth"
        }
    }
}
