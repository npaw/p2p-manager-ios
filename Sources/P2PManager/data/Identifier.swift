//
//  Identifier.swift
//  P2PManager
//
//  Created by Elisabet Massó on 18/5/22.
//  Copyright © 2022 NPAW. All rights reserved.
//

import Foundation

public class Identifier: NSObject {
    
    public private(set) var id: Int16
    
    public init(id: UInt16) {
        self.id = Int16(bitPattern: id)
    }
    
    public init(id: Int16) {
        self.id = id
    }
    
    public static func fromData(_ data: Data) -> Identifier {
        let id = data[0] << 8 | data[1] & 0xFF
        let formattedId = Int16(Int8(bitPattern: id))
        return Identifier(id: formattedId)
    }
    
    public func toByteArray() -> Data {
        var data = Data(repeating: 0, count: 2)
        data[0] = UInt8((id >> 8) & 0xff)
        data[1] = UInt8(id & 0xff)
        return data
    }
    
    static func == (lhs: Identifier, rhs: Identifier) -> Bool {
        return lhs.id == rhs.id
    }
    
}

