//
//  PeerConnection.swift
//  P2PManager
//
//  Created by Elisabet Massó on 7/2/22.
//  Copyright © 2022 NPAW. All rights reserved.
//

import Foundation
import WebRTC
import Utils

public enum PeerConnectionEvent {
    case connected
    case disconnected
    case failed
    case sendToTracker
    case emit
}

enum DataChannelEvent {
    case data
    case channelOpen
}

public protocol PeerConnectionDelegate {
    func peerConnectioDidReceiveEvent(_ event: PeerConnectionEvent, id: String, message: Message?, for peer: PeerConnection?)
    func connect()
    func didReceiveData(_ data: Data, for peer: PeerConnection)
    func didReceiveMessage(_ data: Data, for peer: PeerConnection)
}

extension PeerConnectionDelegate {
    public func connect() { }
    public func didReceiveData(_ data: Data, for peer: PeerConnection) { }
    public func didReceiveMessage(_ data: Data, for peer: PeerConnection) { }
    public func peerConnectioDidReceiveEvent(_ event: PeerConnectionEvent, id: String, message: Message?, for peer: PeerConnection?) { }
}

public class PeerConnection: NSObject {
    
    private var delegates = [PeerConnectionDelegate]()
            
    private var offerId: String
    private var isInitiator: Bool
    private var iceServers: [RTCIceServer]
//    private var delegate: PeerConnectionDelegate?
    
    enum PeerConnectionStatus {
        case new
        case connecting
        case connected
        case disconnected
        case failed
        case closed
    }
    
    // Own id
    public var id: String?
    
    // WebRTC properties
    private var localPeerConnection: RTCPeerConnection?
    private var localDataChannel: RTCDataChannel?
    private var remoteDataChannel: RTCDataChannel?
    private var localSdp: RTCSessionDescription?
    private var localInfoHash: String?
    private var sdpMediaConstraints: RTCMediaConstraints?
    private var iceCandidatesQueue = [RTCIceCandidate]()
    private var localCandidates = [RTCIceCandidate]()
    
    private var connectionStatus: PeerConnectionStatus = .new
    private var addIceImmediately = false
    
    /// Reference for Video segments in this case dataSpecKeys
    var remoteKeys = [String]()
        
    init(offerId: String, isInitiator: Bool, iceServers: [RTCIceServer], delegate: PeerConnectionDelegate) {
        self.offerId = offerId
        self.isInitiator = isInitiator
        self.iceServers = iceServers
//        self.delegate = delegate
        delegates.append(delegate)
    }
    
    public func add(delegate: PeerConnectionDelegate) {
        delegates.append(delegate)
    }
    
    func connect(factory: RTCPeerConnectionFactory, infoHash: String, isReconnecting: Bool) {
        createPeerConnection(with: factory)
        if isInitiator {
            createDataChannel()
        }
        localInfoHash = infoHash
    }
    
    public func hasSegment(_ segment: String) -> Bool {
        return remoteKeys.contains(segment)
    }
    
    public func setRemoteKeys(_ keys: [String]) {
        remoteKeys = keys
    }
    
    func resetRemoteKeys() {
        remoteKeys.removeAll()
    }
    
    private func createPeerConnection(with factory: RTCPeerConnectionFactory) {
        let rtcConfig = RTCConfiguration()
        rtcConfig.iceServers = iceServers
        sdpMediaConstraints = RTCMediaConstraints(mandatoryConstraints: ["iceRestart" : "true"], optionalConstraints: nil)
        localPeerConnection = factory.peerConnection(with: rtcConfig, constraints: sdpMediaConstraints!, delegate: self) // TODO: - Check if these constraints are correct
    }
    
    /// A `DataChannel` can be used for bidirectional peer-to-peer transfers of arbitrary data.
    private func createDataChannel() {
        let config = RTCDataChannelConfiguration()
        localDataChannel = localPeerConnection?.dataChannel(forLabel: "dataChannel", configuration: config)
        localDataChannel?.delegate = self
    }
    
    func createOffer(completion: @escaping (Offer?) -> Void) {
        localPeerConnection?.offer(for: sdpMediaConstraints!, completionHandler: { [self] sdp, error in
            guard let sdp = sdp, error == nil else {
                Log.error(tag: self, "Could not create local sdp.")
                return
            }
            Log.debug(tag: self, "Created local offer.")
            localSdp = sdp
            setLocalDescription(with: sdp) { error in
                guard error == nil else {
                    Log.error(tag: self, "Could not create offer local description.")
                    return
                }
                completion(Offer(id: self.offerId, sdp: self.localSdp?.sdp ?? ""))
            }
            
        })
    }
    
    func addIceCandidate(_ candidate: RTCIceCandidate, completion: @escaping (Error?) -> ()) {
        if addIceImmediately {
            localPeerConnection?.add(candidate, completionHandler: completion)
        } else {
            iceCandidatesQueue.append(candidate)
        }
    }
    
    private func flushCandidatesQueue(completion: @escaping () -> Void) {
        Log.debug(tag: self, "Flushing candidates.")
        let group = DispatchGroup() // TODO: Change to a background thread
        for candidate in iceCandidatesQueue {
            group.enter()
            localPeerConnection?.add(candidate) { error in
                if error != nil {
                    Log.error(tag: self, "Error adding candidate \(candidate.description)")
                }
                group.leave()
            }
        }
        addIceImmediately = true
        completion()
    }
    
    func setLocalDescription(with sdp: RTCSessionDescription, completion: @escaping (Error?) -> Void) {
        localPeerConnection?.setLocalDescription(sdp, completionHandler: { error in
            guard error == nil else {
                completion(error)
                return
            }
            if self.isInitiator {
                // Setting local description after creating offer
                if self.localPeerConnection?.remoteDescription == nil {
                    Log.debug(tag: self, "Setting local description after creating offer.")
                    completion(nil)
                } else {
                    Log.debug(tag: self, "Setting remote description after receiving answer.")
                    self.flushCandidatesQueue {
                        completion(nil)
                    }
                }
            } else {
                // Setting remote desc after receiving offer
                if self.localPeerConnection?.localDescription == nil {
                    // Create answer outside this
                    Log.debug(tag: self, "Setting remote description after receiving offer.")
                    completion(nil)
                } else {
                    Log.debug(tag: self, "Setting local description after creating answer.")
                    self.flushCandidatesQueue {
                        completion(nil)
                    }
                }
            }
        })
    }
    
    func setRemoteDescription(_ sdp: RTCSessionDescription, completion: @escaping (Error?) -> Void) {
        localPeerConnection?.setRemoteDescription(sdp, completionHandler: completion)
    }
    
    func createAnswer(completion: @escaping (_ sdp: RTCSessionDescription?) -> Void) {
        guard let sdpMediaConstraints = sdpMediaConstraints else { return }
        localPeerConnection?.answer(for: sdpMediaConstraints) { sdp, error in
            guard let sdp = sdp, error == nil else {
                Log.error(tag: self, "Could not create answer.")
                completion(nil)
                return
            }
            self.setLocalDescription(with: sdp) { error in
                guard error == nil else {
                    Log.error(tag: self, "Could not create answer local description.")
                    completion(nil)
                    return
                }
                completion(sdp)
            }
            
        }
    }
    
    func isConnected() -> Bool {
        return connectionStatus == .connected
    }
    
    public func sendMessage(_ msg: Data) {
        var prefix = Data(count: 4)
        prefix[0] = DataType.message.byteValue()
        send(prefix: prefix, data: msg, isBinary: false)
    }
    
    public func sendData(_ data: Data, id: Identifier) {
        var prefix = Data(count: 4)
        prefix[0] = DataType.data.byteValue()
        prefix[1] = id.toByteArray()[0]
        prefix[2] = id.toByteArray()[1]
        send(prefix: prefix, data: data, isBinary: true)
    }
    
    func sendDataType(_ dataType: DataType, data: Data) {
        var prefix = Data(count: 4)
        prefix[0] = dataType.byteValue()
        send(prefix: prefix, data: data, isBinary: (dataType == .bandwidth || dataType == .data) ? true : false)
    }
    
    private func send(prefix: Data, data: Data?, isBinary: Bool) {
        guard prefix.count == 4, let data = data else {
            Log.error(tag: self, "Prefix needs a fixed size of 4.")
            return
        }
        var newData = Data(capacity: prefix.count + data.count) // TODO: Should be `Data(count: x)`?
        newData.append(prefix)
        newData.append(data)
        if isInitiator {
            localDataChannel?.sendData(RTCDataBuffer(data: newData, isBinary: isBinary))
        } else {
            remoteDataChannel?.sendData(RTCDataBuffer(data: newData, isBinary: isBinary))
        }
    }
    
    public func close() {
        localPeerConnection?.close()
        localInfoHash = nil // TODO: Reset infoHash here?
    }
    
    func bandwidthTest() {
        let randomBytes = Data(count: Constants.maxMsgSize).map { _ in
            UInt8.random(in: 0...UInt8.max)
        }
        sendDataType(.bandwidth, data: Data(randomBytes))
    }
    
}

extension PeerConnection: RTCPeerConnectionDelegate {
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange stateChanged: RTCSignalingState) {
        Log.debug(tag: self, "Signaling state changed to \(stateChanged).")
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didAdd stream: RTCMediaStream) {
        Log.debug(tag: self, "Media stream added.")
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didRemove stream: RTCMediaStream) {
        Log.debug(tag: self, "Media stream removed.")
    }
    
    public func peerConnectionShouldNegotiate(_ peerConnection: RTCPeerConnection) {
        Log.debug(tag: self, "Should negotiate.")
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceConnectionState) {
        Log.debug(tag: self, "Ice connection state changed to \(newState).")
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceGatheringState) {
        Log.debug(tag: self, "Ice gathering state changed to \(newState).")
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didGenerate candidate: RTCIceCandidate) {
        localCandidates.append(candidate)
        
        var message = Message()
        message.action = .candidates
        message.offer_id = offerId
        message.info_hash = localInfoHash // TODO: Reset infoHash somewhere?
        
        if let id = id {
            message.to_peer_id = id
        }
        
        var localCandidate = Candidate()
        localCandidate.type = "candidate"
        localCandidate.candidate = candidate.sdp
        localCandidate.id = candidate.sdpMid
        localCandidate.label = candidate.sdpMLineIndex
        
        if message.candidates != nil {
            message.candidates?.append(localCandidate)
        } else {
            message.candidates = [localCandidate]
        }
        
        Log.debug(tag: self, "Generated new ice candidate.")

        for delegate in delegates {
            delegate.peerConnectioDidReceiveEvent(.sendToTracker, id: id ?? "", message: message, for: self)
        }
//        delegate?.peerConnectioDidReceiveEvent(.sendToTracker, id: id ?? "", message: message)
    }

    public func peerConnection(_ peerConnection: RTCPeerConnection, didRemove candidates: [RTCIceCandidate]) {
        Log.debug(tag: self, "Removed \(candidates.count) ice candidates.")
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didOpen dataChannel: RTCDataChannel) {
        Log.debug(tag: self, "Data channel did open with state: \(dataChannel.readyState).")
        remoteDataChannel = dataChannel
        remoteDataChannel?.delegate = self
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didChangeStandardizedIceConnectionState newState: RTCIceConnectionState) {
        Log.debug(tag: self, "Standardized ice connection did change with state: \(newState).")
    }
    
    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCPeerConnectionState) {
        switch newState {
            case .new:
                connectionStatus = .new
            case .connecting:
                connectionStatus = .connecting
            case .connected:
                connectionStatus = .connected
                if let id = id {
                    for delegate in delegates {
                        delegate.peerConnectioDidReceiveEvent(.connected, id: id, message: nil, for: self)
                    }
                }
            case .disconnected:
                connectionStatus = .disconnected
                if let id = id {
                    for delegate in delegates {
                        delegate.peerConnectioDidReceiveEvent(.disconnected, id: id, message: nil, for: self)
                    }
                }
            case .failed:
                connectionStatus = .failed
                if let id = id {
                    for delegate in delegates {
                        delegate.peerConnectioDidReceiveEvent(.disconnected, id: id, message: nil, for: self)
                    }
                }
            case .closed:
                connectionStatus = .closed
            @unknown default:
                Log.error(tag: self, "Non existing state: \(newState).")
        }
        
        Log.debug(tag: self, "Peer connection state did change with state: \(newState).")

    }
    
}

extension PeerConnection: RTCDataChannelDelegate {
    
    public func dataChannelDidChangeState(_ dataChannel: RTCDataChannel) {
        Log.debug(tag: self, "DataChannel changed state to \(dataChannel.readyState).")
        if dataChannel.readyState == .open {
            bandwidthTest()
            for delegate in delegates {
                delegate.connect()
            }
        }
    }
    
    public func dataChannel(_ dataChannel: RTCDataChannel, didReceiveMessageWith buffer: RTCDataBuffer) {
        var prefix = Array<Int8>(repeating: 0, count: Constants.dataPrefixSize/MemoryLayout<Int8>.stride)
        _ = prefix.withUnsafeMutableBytes { buffer.data.copyBytes(to: $0) }
        
        let data = buffer.data[Constants.dataPrefixSize..<buffer.data.count]
        
        let dataType = DataType(rawValue: prefix[0])
        Log.debug(tag: self, "DataChannel did receive \(dataType?.description ?? "nil").")

        dataChannelDidReceiveData(data, for: dataType)
        
    }
    
}

extension PeerConnection {
    
    func dataChannelDidReceiveData(_ data: Data, for dataType: DataType?) {
        switch dataType {
            case .data:
                for delegate in delegates {
                    delegate.didReceiveData(data, for: self)
                }
            
            case .message:
                for delegate in delegates {
                    delegate.didReceiveMessage(data, for: self)
                }

            case .bandwidth:
                Log.debug(tag: self, "Received a bandwidth test message.")
            
            default:
                Log.warn(tag: self, "Received an unknown message!")
        }
    }
    
}
