//
//  StringProtocol+Extension.swift
//  P2PManager
//
//  Created by Elisabet Massó on 9/2/22.
//  Copyright © 2022 NPAW. All rights reserved.
//

import Foundation

extension StringProtocol {

    var data: Data { .init(utf8) }
    var bytes: [UInt8] { .init(utf8) }

}
