//
//  Constants.swift
//  P2PManager
//
//  Created by Elisabet Massó on 20/5/22.
//  Copyright © 2022 NPAW. All rights reserved.
//

import Foundation
import WebRTC

struct Constants {
    
    static let dataPrefixSize = 4
    static let maxMsgSize = 64 * 1024 - dataPrefixSize
    
    static let announcePeers = 5
    
    static var iceServers: [RTCIceServer] {
        get {
            let iceServer = RTCIceServer(urlStrings: [
                "stun:stun.l.google.com:19302",
                "stun:stun1.l.google.com:19302",
                "stun:stun2.l.google.com:19302",
                "stun:stun3.l.google.com:19302",
                "stun:stun4.l.google.com:19302"
            ])
            return [iceServer]
        }
    }
    
}
