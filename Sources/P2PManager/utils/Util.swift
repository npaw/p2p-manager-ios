//
//  Util.swift
//  P2PManager
//
//  Created by Elisabet Massó on 7/2/22.
//  Copyright © 2022 NPAW. All rights reserved.
//

import Foundation
import Utils

final public class Util: Utils {
    
    static func randomId() -> String {
        return String(UUID().uuidString.replacingOccurrences(of: "-", with: "").prefix(24))
    }
    
    static func getUrlPath(_ url: String) -> String? {
        guard let url = URL(string: url) else {
            return nil
        }
        return url.path
    }
    
}
