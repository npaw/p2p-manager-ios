//
//  Message.swift
//  P2PManager
//
//  Created by Elisabet Massó on 8/2/22.
//  Copyright © 2022 NPAW. All rights reserved.
//

import Foundation
import WebRTC
import Utils

enum WSActionType: String, Codable {
    case join, offer, offers, answer, candidates, prAnswer, rollback
    
    func toRTCSdpType() -> RTCSdpType? {
        switch self {
            case .offer: return .offer
            case .answer: return .answer
            case .prAnswer: return .prAnswer
            case .rollback: return .rollback
            default:
                Log.debug("Non existing RTCSdpType object type.")
                return nil
        }
    }
}

public struct Message {
    
    var action: WSActionType?
    var info_hash: String? = nil {
        didSet {
            guard let infoHash = info_hash?.prefix(32) else { return }
            info_hash = String(infoHash)
        }
    }
    var peer_id: String?
    var to_peer_id: String?
    var offer_id: String?
    var offers: [SubMessage]?
    var offer: SubMessage?
    var answer: SubMessage?
    var candidates: [Candidate]?
    
    var token: String?
    var account_code: String?
    
}

extension Message: Codable {
    
    init(data: Data) throws {
        self = try JSONDecoder().decode(Message.self, from: data)
    }
    
    init(dictionary: [String : Any]) throws {
        self = try JSONDecoder().decode(Message.self, from: JSONSerialization.data(withJSONObject: dictionary))
    }
    
    private enum CodingKeys: String, CodingKey {
        case action, info_hash, peer_id, to_peer_id, offer_id, offers, offer, answer, candidates, token, account_code
    }
    
}
