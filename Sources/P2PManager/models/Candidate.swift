//
//  Candidate.swift
//  P2PManager
//
//  Created by Elisabet Massó on 10/2/22.
//  Copyright © 2022 NPAW. All rights reserved.
//

import Foundation

struct Candidate {
    
    var type: String?
    var candidate: String?
    var id: String?
    var label: Int32?
    
}

extension Candidate: Codable {
    
    init(data: Data) throws {
        self = try JSONDecoder().decode(Candidate.self, from: data)
    }
    
    init(dictionary: [String : Any]) throws {
        self = try JSONDecoder().decode(Candidate.self, from: JSONSerialization.data(withJSONObject: dictionary))
    }
    
    private enum CodingKeys: String, CodingKey {
        case type, candidate, id, label
    }
    
}
