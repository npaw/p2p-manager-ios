//
//  Candidate.swift
//  P2PManager
//
//  Created by Elisabet Massó on 19/5/22.
//  Copyright © 2022 NPAW. All rights reserved.
//

import Foundation

struct ActiveDownload {
    
    var id: String?
    var data: [Data]?
    var startTime: Double
    var size: Int?
    
    init() {
        id = nil
        data = nil
        startTime = 0
        size = nil
    }
    
}
