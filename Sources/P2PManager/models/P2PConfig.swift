//
//  Candidate.swift
//  P2PManager
//
//  Created by Elisabet Massó on 18/5/22.
//  Copyright © 2022 NPAW. All rights reserved.
//

import Foundation
import WebRTC

public struct P2PConfig {
    
    public var tracker: String?
    public var accountCode: String?
    public var iceServers: [RTCIceServer]?
    public var announcePeers: Int?
    public var maxAnnouncePeers: Int?
    public var token: String?
    
    public init() {
        tracker = nil
        accountCode = nil
        iceServers = nil
        announcePeers = nil
        maxAnnouncePeers = nil
        token = nil
    }
    
}
