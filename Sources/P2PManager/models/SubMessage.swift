//
//  Message.swift
//  P2PManager
//
//  Created by Elisabet Massó on 18/5/22.
//  Copyright © 2022 NPAW. All rights reserved.
//

import Foundation

struct SubMessage {
    
    var type: WSActionType?
    var offer_id: String?
    var sdp: String?
    
}

extension SubMessage: Codable {
    
    init(dictionary: [String : Any]) throws {
        self = try JSONDecoder().decode(SubMessage.self, from: JSONSerialization.data(withJSONObject: dictionary))
    }
    
    private enum CodingKeys: String, CodingKey {
        case type, offer_id, sdp
    }
    
}
