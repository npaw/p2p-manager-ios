//
//  Offer.swift
//  P2PManager
//
//  Created by Elisabet Massó on 7/2/22.
//  Copyright © 2022 NPAW. All rights reserved.
//

import Foundation

struct Offer {
    
    var id: String
    var sdp: String
    
}
