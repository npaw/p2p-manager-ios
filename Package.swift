// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "P2PManager",
    platforms: [
        .iOS(.v10),
        .macOS(.v10_15)
    ],
    products: [
        .library(
            name: "P2PManager",
            targets: ["P2PManager", "WebRTC"]),
    ],
    dependencies: [
        .package(url: "https://github.com/daltoniam/Starscream.git", .upToNextMajor(from: "3.1.1")),
        .package(url: "https://bitbucket.org/npaw/sdk-utils-ios.git", .upToNextMajor(from: "1.0.0"))
    ],
    targets: [
        .target(
            name: "P2PManager",
            dependencies: [
                "WebRTC",
                "Starscream",
                .product(name: "Utils", package: "sdk-utils-ios"),
            ]),
        .binaryTarget(
            name: "WebRTC",
            path: "Sources/Frameworks/WebRTC.xcframework")
    ],
    swiftLanguageVersions: [
        .v5
    ]
)
