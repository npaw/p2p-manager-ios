# Changelog

## [0.0.2] - 2022-13-20
### Updated
- NPAWUtilsSDK package version

## [0.0.1] - 2022-08-23
### Added
- Release version
