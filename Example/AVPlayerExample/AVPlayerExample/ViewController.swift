//
//  ViewController.swift
//  AVPlayerExample
//
//  Created by Elisabet Massó on 14/10/21.
//  Copyright © 2021 NPAW. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import P2PManager

class ViewController: UIViewController {
    
    var p2pManager: P2PManager?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        p2pManager = P2PManager(accountCode: "demoyoubora", token: "tpm")
        p2pManager?.start()
        
    }
    
    @IBAction func playVideo(_ sender: Any) {
        guard /*let balancer = balancer,*/ let url = URL(string: /*balancer.getResource(url: */"http://multiplatform-f.akamaihd.net/i/multi/will/bunny/big_buck_bunny_,640x360_400,640x360_700,640x360_1000,950x540_1500,.f4v.csmil/master.m3u8")/*)*/ else {
            return
        }
        p2pManager?.join(mediaUrl: url.absoluteString)

//        // Create an AVPlayer, passing it the HTTP Live Streaming URL.
//        let player = AVPlayer(url: url)
//
//        // Create a new AVPlayerViewController and pass it a reference to the player.
//        let controller = AVPlayerViewController()
//        controller.player = player
//
//        // Modally present the player and call the player's play() method when complete.
//        present(controller, animated: true) {
//            player.play()
//        }
        
    }


}

